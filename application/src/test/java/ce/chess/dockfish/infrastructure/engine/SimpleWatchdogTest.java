package ce.chess.dockfish.infrastructure.engine;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SimpleWatchdogTest {

  @Mock
  private BooleanSupplier condition;

  @Mock
  private Runnable actionWhenFalse;

  private SimpleWatchdog cut;

  @BeforeEach
  void setup() {
    cut = new SimpleWatchdog();
  }

  @Test
  void blocksWhileRunningThenRunsAction_AndCanBeReused() {
    given(condition.getAsBoolean()).willReturn(true).willReturn(false);

    cut.watch(condition, 1, actionWhenFalse);
    Awaitility.await().with().atMost(2, TimeUnit.SECONDS).until(cut::waitWhileConditionIsTrue); // blocks

    InOrder inOrder = inOrder(condition, actionWhenFalse);
    inOrder.verify(condition, times(2)).getAsBoolean();
    inOrder.verify(actionWhenFalse).run();

    canBeReused();
  }

  void canBeReused() {
    BooleanSupplier condition2 = mock(BooleanSupplier.class);
    given(condition2.getAsBoolean()).willReturn(true).willReturn(false);
    Runnable actionWhenFalse2 = mock(Runnable.class);

    cut.watch(condition2, 1, actionWhenFalse2);

    verify(actionWhenFalse2, timeout(1500)).run();
  }
}
