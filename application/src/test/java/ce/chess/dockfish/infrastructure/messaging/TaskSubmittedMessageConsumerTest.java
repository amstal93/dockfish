package ce.chess.dockfish.infrastructure.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import ce.chess.commontech.messaging.Message;
import ce.chess.dockfish.application.InfiniteAnalysisService;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.eclipse.microprofile.config.Config;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskSubmittedMessageConsumerTest {

  @Mock
  private InfiniteAnalysisService service;

  @Mock
  private ObjectMapper objectMapper;

  @Mock
  private Config config;

  @InjectMocks
  private TaskSubmittedMessageConsumer cut;

  @Test
  void doesWaitAndCallService() throws IOException {
    SubmitTaskCommand command = mock(SubmitTaskCommand.class);
    given(objectMapper.readValue(new byte[]{42}, SubmitTaskCommand.class)).willReturn(command);
    given(service.startSync(command)).willReturn(Optional.of(ImmutableTaskId.of("task")));
    given(config.getOptionalValue("task_consumer_delay_seconds", Long.class)).willReturn(Optional.of(1L));

    cut.handleMessage(new Message().withBody(new byte[]{42}));

    verify(service).startSync(command);
  }

  @Test
  void failsIfMessageCannotBeMapped() throws IOException {
    given(objectMapper.readValue(new byte[]{42}, SubmitTaskCommand.class)).willThrow(new IOException());

    Message message = new Message().withBody(new byte[]{42});
    assertThrows(UncheckedIOException.class, () -> cut.handleMessage(message));
  }

  @Test
  void hasBindingParameters() {
    assertThat(cut.getExchangeName(), is(not(blankOrNullString())));
    assertThat(cut.getQueueNames(), is(not(empty())));
    verify(config).getOptionalValue("queue_name_tasks", String.class);
    assertThat(cut.getExchangeType(), is("TOPIC"));
    assertThat(cut.isAutoAck(), is(false));
  }

  @Test
  void returnsPrefetchCountOne() {
    assertThat(cut.getPrefetchLimit(), is(1));
  }

  @Test
  void hasDefaultRoutingKey() {
    given(config.getOptionalValue("routing_key_task", String.class)).willReturn(Optional.empty());
    assertThat(cut.getRoutingKeysBinding(), contains(List.of("#")));
  }

  @Test
  void hasSingleRoutingKeyFromEnvironmentVariable() {
    given(config.getOptionalValue("routing_key_task", String.class)).willReturn(Optional.of("anyKey"));
    assertThat(cut.getRoutingKeysBinding(), contains(List.of("anyKey")));
  }

  @Test
  void hasListOfRoutingKeysFromEnvironmentVariable() {
    given(config.getOptionalValue("routing_key_task", String.class))
        .willReturn(Optional.of("anyKey1,anyKey2,anyKey3;;anyKey4"));

    List<List<String>> actual = cut.getRoutingKeysBinding();

    assertThat(actual,
        contains(List.of("anyKey1", "anyKey2", "anyKey3"), List.of(""), List.of("anyKey4")));
  }

  @Test
  void hasConsumerArgsFromEnvironmentVariable() {
    given(config.getOptionalValue("consumer_arguments_task", String.class))
        .willReturn(Optional.of("anyKey1,10,anyKey3,anyKey4"));

    Map<String, Object> actual = cut.getConsumerArguments();

    assertThat(actual, is(Map.of("anyKey1", 10, "anyKey3", "anyKey4")));
  }

  @Test
  void hasNoConsumerArgsFromEmptyEnvironmentVariable() {
    given(config.getOptionalValue("consumer_arguments_task", String.class))
        .willReturn(Optional.empty());

    Map<String, Object> actual = cut.getConsumerArguments();

    assertThat(actual, is(Map.of()));
  }
}
