package ce.chess.dockfish.infrastructure.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import ce.chess.commontech.messaging.Message;
import ce.chess.commontech.messaging.rabbit.RabbitSender;
import ce.chess.dockfish.application.EvaluationMessageService;
import ce.chess.dockfish.domain.event.AnalysisFinished;
import ce.chess.dockfish.domain.model.result.EvaluationMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Optional;
import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import nl.altindag.log.LogCaptor;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
class FinalResultPublisherTest {
  @WeldSetup
  private WeldInitiator weld = WeldInitiator
      .from(FinalResultPublisher.class, MockProducer.class)
      .build();

  private static final String MESSAGE_BODY = "messageContent";

  static EvaluationMessageService resultService = mock(EvaluationMessageService.class);

  static ObjectMapper objectMapper = mock(ObjectMapper.class);

  static Event<PublishFailed> publishFailedEvent = mock(Event.class);

  static RabbitSender rabbitSender = mock(RabbitSender.class);

  private static LogCaptor logCaptor;

  @BeforeAll
  public static void setupLogCaptor() {
    logCaptor = LogCaptor.forClass(FinalResultPublisher.class);
  }

  @AfterEach
  public void clearLogCaptor() {
    logCaptor.clearLogs();
  }

  @AfterAll
  public static void closeLogCaptor() {
    logCaptor.resetLogLevel();
    logCaptor.close();
  }

  @Priority(99)
  static class MockProducer {
    @Produces
    @Alternative
    EvaluationMessageService resultService() {
      return resultService;
    }

    @Produces
    @Alternative
    ObjectMapper objectMapper() {
      return objectMapper;
    }

    @Produces
    @Alternative
    Event<PublishFailed> publishFailedEvent() {
      return publishFailedEvent;
    }

    @Produces
    @Alternative
    RabbitSender rabbitSender() {
      return rabbitSender;
    }
  }

  @BeforeEach
  void setUp() {
    Mockito.reset(resultService, objectMapper, publishFailedEvent, rabbitSender);
  }

  @Mock
  private AnalysisFinished publishEvent;

  @Mock
  private EvaluationMessage evaluationMessage;

  @InjectMocks
  private FinalResultPublisher cut;

  private Message expectedMessage = new Message()
      .withExchange("evaluation.created")
      .withRoutingKey("")
      .withContentType("application/json")
      .asPersistent()
      .withBody(MESSAGE_BODY);

  private void fire(AnalysisFinished event) {
    weld.event().select(AnalysisFinished.class).fire(event);
  }
  //

  @Test
  void hasBindingParameters() {
    assertThat(cut.getExchangeName(), is(not(blankOrNullString())));
    assertThat(cut.getQueueName(), is(not(blankOrNullString())));
  }

  @Test
  void publishesMessage() throws IOException {
    given(resultService.getLastEvaluationMessage(any())).willReturn(Optional.of(evaluationMessage));
    given(objectMapper.writeValueAsString(evaluationMessage)).willReturn(MESSAGE_BODY);

    fire(publishEvent);

    verify(rabbitSender).publish(refEq(expectedMessage));
    verify(rabbitSender).close();
  }

  @Test
  void objectMapperFailsSafeley() throws IOException {
    given(resultService.getLastEvaluationMessage(any())).willReturn(Optional.of(evaluationMessage));
    given(objectMapper.writeValueAsString(evaluationMessage)).willThrow(JsonProcessingException.class);

    performUnlogged(() -> fire(publishEvent));

    verifyNoInteractions(rabbitSender);
  }


  @Test
  void publishFailsSafelyAndFires() throws IOException {
    given(resultService.getLastEvaluationMessage(any())).willReturn(Optional.of(evaluationMessage));
    given(objectMapper.writeValueAsString(evaluationMessage)).willReturn(MESSAGE_BODY);
    doThrow(new IOException("Exception from MessagingSetupTest")).when(rabbitSender).publish(any(Message.class));

    performUnlogged(() -> cut.publishFinalEvaluation(publishEvent));

    verify(publishFailedEvent).fire(any());
    verify(publishFailedEvent).fire(ImmutablePublishFailed.of("evaluation.created", MESSAGE_BODY));
  }


  @Test
  void givenNoEvaluationMessageThenJustLog() {
    given(resultService.getLastEvaluationMessage(any())).willReturn(Optional.empty());

    cut.publishFinalEvaluation(publishEvent);

    verifyNoInteractions(rabbitSender);
  }

  private static void performUnlogged(Runnable function) {
    logCaptor.disableLogs();
    function.run();
    logCaptor.resetLogLevel();
  }
}
