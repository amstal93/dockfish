package ce.chess.dockfish.application;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import ce.chess.dockfish.domain.model.staticevaluation.ImmutableStaticEvaluation;
import ce.chess.dockfish.domain.model.staticevaluation.ImmutableStaticEvaluationRequest;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluation;
import ce.chess.dockfish.infrastructure.engine.EngineController;

import javax.enterprise.event.Event;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StaticEvaluationServiceTest {
  @Mock
  EngineController engineController;

  @Mock
  Event<StaticEvaluation> resultPublisher;

  @InjectMocks
  StaticEvaluationService cut;

  @Test
  void delegatesToEngineController() {
    given(engineController.retrieveStaticEvaluation(anyString())).willReturn("result");

    ImmutableStaticEvaluationRequest evaluationRequest = ImmutableStaticEvaluationRequest.of("anyFen");
    cut.createAndPublishEvaluation(evaluationRequest);

    InOrder inOrder = Mockito.inOrder(engineController, resultPublisher);
    inOrder.verify(engineController).acquireLock();
    inOrder.verify(engineController).retrieveStaticEvaluation("anyFen");
    inOrder.verify(resultPublisher).fire(ImmutableStaticEvaluation.of(evaluationRequest, "result"));
    inOrder.verify(engineController).releaseLock();
  }

}
