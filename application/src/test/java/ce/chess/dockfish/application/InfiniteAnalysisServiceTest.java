package ce.chess.dockfish.application;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import ce.chess.commontech.messaging.RequeueException;
import ce.chess.dockfish.application.command.ImmutableEngineOption;
import ce.chess.dockfish.application.command.ImmutableSubmitTaskCommand;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.application.command.UciOptionsConfiguration;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.model.task.TaskRepository;
import ce.chess.dockfish.infrastructure.engine.EngineController;
import ce.chess.dockfish.infrastructure.engine.EngineListener;

import java.util.List;
import java.util.Optional;
import org.eclipse.microprofile.config.Config;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InfiniteAnalysisServiceTest {

  public static final ImmutableEngineOption LOCAL_OPTION_HASH = ImmutableEngineOption.of("Hash", "HashValue");
  private static final ImmutableEngineOption CALLER_OPTION_DEFAULT =
      ImmutableEngineOption.of("name", "value");
  private static final ImmutableEngineOption CALLER_OPTION_HASH =
      ImmutableEngineOption.of("Hash", "CallerHashValue");
  public static final ImmutableEngineOption LOCAL_OPTION_THREADS = ImmutableEngineOption.of("Threads", "ThreadsValue");
  public static final ImmutableEngineOption LOCAL_OPTION_SYZYGY_PATH = ImmutableEngineOption.of("SyzygyPath", "/syz");

  @Mock
  private EngineController engineController;

  @Mock
  private TaskRepository taskRepository;

  @Mock
  private EngineListener engineListener;

  @Mock
  private UciOptionsConfiguration uciOptionsConfiguration;

  @Mock
  Config config;

  @Captor
  private ArgumentCaptor<EngineAnalysisRequest> analysisRequest;

  @InjectMocks
  private InfiniteAnalysisService cut;

  private final SubmitTaskCommand command = ImmutableSubmitTaskCommand.builder()
      .name("someName")
      .engineId("engineId")
      .pgn("1. e4 e5")
      .initialPv(3)
      .maxDepth(2)
      .options(List.of(CALLER_OPTION_DEFAULT, CALLER_OPTION_HASH))
      .build();

  @Nested
  class WhenCalledViaRest {
    @Nested
    class AndEngineIsReady {
      @Mock
      private EngineAnalysisRequest engineAnalysisRequestFromController;

      @BeforeEach
      void setUp() {
        given(engineController.tryAcquireLock()).willReturn(true);
        given(engineController.startAnalysis(any(), any())).willReturn(engineAnalysisRequestFromController);
      }

      @Test
      void thenTaskIdIsReturned() {
        Optional<TaskId> taskId = cut.startAsync(command);

        assertThat(taskId.isPresent(), is(true));
      }

      @Test
      void thenControllerIsCalledInOrder() {
        cut.startAsync(command);

        InOrder inOrder = Mockito.inOrder(engineController, engineListener, taskRepository);
        inOrder.verify(engineController).tryAcquireLock();
        inOrder.verify(engineController).startAnalysis(analysisRequest.capture(), eq(engineListener));
        inOrder.verify(taskRepository).save(engineAnalysisRequestFromController);
      }

      @Test
      void thenControllerIsCalledWithCorrectAnalysisRequest() {
        Optional<TaskId> taskId = cut.startAsync(command);

        verify(engineController).startAnalysis(analysisRequest.capture(), eq(engineListener));
        EngineAnalysisRequest actualAnalysisRequest = analysisRequest.getValue();
        assertThat(actualAnalysisRequest.taskId(), is(equalTo(taskId.orElseThrow(AssertionError::new))));
        assertThat(actualAnalysisRequest.initialPv(), is(equalTo(command.initialPv())));
        assertThat(actualAnalysisRequest.maxDepth(), is(equalTo(command.maxDepth())));
        assertThat(actualAnalysisRequest.engineOptions(), contains(CALLER_OPTION_DEFAULT, CALLER_OPTION_HASH));
        assertThat(actualAnalysisRequest.uciEngineName(), is(Optional.empty()));
      }

      @Test
      void doesReleaseLockAfterError() {
        given(engineController.startAnalysis(any(), any()))
            .willThrow(IllegalArgumentException.class);

        assertThrows(RequeueException.class, () -> cut.startAsync(command));

        verify(engineController).releaseLock();
      }
    }

    @Nested
    class AndEngineIsAlreadyRunning {

      @BeforeEach
      void setUp() {
        given(engineController.tryAcquireLock()).willReturn(false);
      }

      @Test
      void thenReturnEmptyTaskId() {

        Optional<TaskId> taskId = cut.startAsync(mock(SubmitTaskCommand.class));

        assertThat(taskId.isPresent(), is(false));
        verify(engineController).tryAcquireLock();
        verifyNoMoreInteractions(engineController);
      }
    }

    @Nested
    class AndTaskIsDuplicate {

      @BeforeEach
      void setUp() {
        given(engineController.tryAcquireLock()).willReturn(true);
        given(taskRepository.hasDuplicate(any())).willReturn(true);
      }

      @Test
      void thenReturnEmptyTaskId() {

        Optional<TaskId> taskId = cut.startAsync(command);

        assertThat(taskId.isPresent(), is(false));
        verify(taskRepository).hasDuplicate(any());
        verify(engineController).tryAcquireLock();
        verify(engineController).releaseLock();
        verifyNoMoreInteractions(engineController);
      }
    }

    @Nested
    class GivenLocalOptions {
      @BeforeEach
      void setUp() {
        given(uciOptionsConfiguration.getLocalEngineOptions())
            .willReturn(List.of(LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS, LOCAL_OPTION_SYZYGY_PATH));
      }

      @Test
      void thenDelegateWithMergedOptions() {
        given(engineController.tryAcquireLock()).willReturn(true);

        Optional<TaskId> taskId = cut.startAsync(command);

        assertThat(taskId.isPresent(), is(true));
        verify(engineController).startAnalysis(analysisRequest.capture(), eq(engineListener));
        assertThat(analysisRequest.getValue().engineOptions(),
            containsInAnyOrder(CALLER_OPTION_DEFAULT, LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS));
      }

      @Test
      void thenDelegateWithMergedOptionsWithSyzygyPath() {
        given(engineController.tryAcquireLock()).willReturn(true);

        Optional<TaskId> taskId = cut.startAsync(ImmutableSubmitTaskCommand.copyOf(command)
            .withUseSyzygyPath(true));

        assertThat(taskId.isPresent(), is(true));
        verify(engineController).startAnalysis(analysisRequest.capture(), eq(engineListener));
        assertThat(analysisRequest.getValue().engineOptions(),
            containsInAnyOrder(CALLER_OPTION_DEFAULT, LOCAL_OPTION_HASH, LOCAL_OPTION_THREADS,
                LOCAL_OPTION_SYZYGY_PATH));
      }
    }
  }

  @Nested
  class WhenCalledViaMessaging {

    @Nested
    class AndTaskIsNotDuplicate {
      @Test
      void thenDelegateToController() {
        EngineAnalysisRequest resultFromController = mock(EngineAnalysisRequest.class);
        given(engineController.startAnalysis(any(), any())).willReturn(resultFromController);
        given(config.getOptionalValue("hostname", String.class)).willReturn(Optional.of("testhost"));

        Optional<TaskId> taskId = cut.startSync(command);

        assertThat(taskId.isPresent(), is(true));

        InOrder inOrder = Mockito.inOrder(engineController, engineListener, taskRepository);
        inOrder.verify(engineController).acquireLock();
        inOrder.verify(engineController).startAnalysis(analysisRequest.capture(), eq(engineListener));
        inOrder.verify(taskRepository).save(resultFromController);
        inOrder.verify(engineController).blockWhileActive();
        assertThat(analysisRequest.getValue().taskId(), is(equalTo(taskId.orElseThrow(AssertionError::new))));
        assertThat(analysisRequest.getValue().taskId(), is(equalTo(taskId.get())));
        assertThat(analysisRequest.getValue().initialPv(), is(equalTo(command.initialPv())));
        assertThat(analysisRequest.getValue().maxDepth(), is(equalTo(command.maxDepth())));
        assertThat(analysisRequest.getValue().engineOptions(), is(equalTo(command.options())));
        assertThat(analysisRequest.getValue().uciEngineName(), is(Optional.empty()));
        assertThat(analysisRequest.getValue().hostname(), is("testhost"));
      }

      @Test
      void doesReleaseLockAfterError() {
        given(engineController.startAnalysis(any(), any()))
            .willThrow(IllegalArgumentException.class);

        assertThrows(RequeueException.class, () -> cut.startSync(command));

        verify(engineController).releaseLock();
      }
    }

    @Nested
    class AndTaskIsKnown {

      @BeforeEach
      void setUp() {
        given(taskRepository.hasDuplicate(any())).willReturn(true);
      }

      @Test
      void thenReturnEmptyTaskId() {

        Optional<TaskId> taskId = cut.startSync(command);

        assertThat(taskId.isPresent(), is(false));
        verify(taskRepository).hasDuplicate(any());
        verifyNoInteractions(engineController);
      }
    }
  }

  @Test
  void stopAnalysisDelegatesToEngineController() {
    given(engineController.uciEngineIsRunning()).willReturn(false);

    boolean result = cut.stopAnalysis();

    verify(engineController).stop();
    assertThat(result, is(true));
  }

  @Test
  void getTaskDetailsDelegatesToRepository() {
    TaskId taskId = ImmutableTaskId.of("42");
    EngineAnalysisRequest engineAnalysisRequest = mock(EngineAnalysisRequest.class);
    given(taskRepository.findByTaskId(taskId)).willReturn(Optional.of(engineAnalysisRequest));

    EngineAnalysisRequest taskDetails = cut.getTaskDetails(taskId);

    verify(taskRepository).findByTaskId(taskId);
    assertThat(taskDetails, is(equalTo(engineAnalysisRequest)));
  }

}
