package ce.chess.dockfish.application.command;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;

import io.smallrye.config.SmallRyeConfigProviderResolver;
import io.smallrye.config.inject.ConfigProducer;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

class UciOptionsConfigurationTest {

  @BeforeAll
  static void beforeAll() {
    ConfigProviderResolver.setInstance(new SmallRyeConfigProviderResolver());
    // make sure that RunningAppConfigResolver is not found, because
    // io.quarkus.test.junit.RunningAppConfigResolver.getConfig().unwrap()
    // will throw UnsupportedOperationException in io.smallrye.config.inject.ConfigProducer.
  }

  @Inject
  UciOptionsConfiguration cut;

  @Nested
  @ExtendWith({WeldJunit5Extension.class})
  class GivenLocalConfigurationIsPresent {
    @WeldSetup
    public WeldInitiator weld = WeldInitiator.from(UciOptionsConfiguration.class, ConfigProducer.class).build();

    @Test
    void thenReturnLocalConfiguration() {
      // read from microprofile-config.properties
      try {
        List<EngineOption> expected = List.of(
            ImmutableEngineOption.of("Hash", "anyHash"),
            ImmutableEngineOption.of("Threads", "anyThreads"),
            ImmutableEngineOption.of("Contempt", "anyContempt"),
            ImmutableEngineOption.of("SyzygyPath", "C:\\tablebases\\3-4-5"),
            ImmutableEngineOption.of("Use NNUE", "true")
        );

        List<EngineOption> actual = cut.getLocalEngineOptions();
        assertThat(actual, containsInAnyOrder(expected.toArray()));
      } catch (Exception ex) { // NOPMD
        ex.printStackTrace(); // NOPMD
      }
    }
  }

  @Nested
  @ExtendWith(MockitoExtension.class)
  class GivenLocalConfigurationIsNotPresent {
    @Mock
    private Config config;

    @InjectMocks
    private UciOptionsConfiguration cutMocked;

    @Test
    void thenReturnEmptyConfigurations() {
      given(config.getPropertyNames()).willReturn(List.of());

      assertThat(cutMocked.getLocalEngineOptions(), is(empty()));
    }
  }
}
