package ce.chess.dockfish.domain.model.engine;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.event.ImmutableEngineInformationReceived;
import ce.chess.dockfish.domain.model.task.ImmutableTaskId;
import ce.chess.dockfish.domain.model.task.TaskId;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ShortLineReplacerTest {

  @Mock
  private EngineInformationReceivedRepository eventRepository;

  @InjectMocks
  private ShortLineReplacer cut;

  @Nested
  class DoNotReplaceLines {

    private EngineInformationReceived event;

    @BeforeEach
    void prepare() {
      event = mock(EngineInformationReceived.class);
      given(event.hasGame()).willReturn(true);
    }

    @Test
    void whenNewLineIsLongEnoughThenItWontBeReplaced() {
      given(event.calculatedPlies()).willReturn(4);

      EngineInformationReceived result = cut.fillUpGameIfTooShort(event);

      assertThat(result, is(equalTo(event)));
      verify(eventRepository, Mockito.never()).findByTaskIdAndStartingWithLineSanMaxOccurredOn(any(), any());
    }

    @Test
    void whenOldEventIsNotFoundThenDontReplace() {
      given(event.calculatedPlies()).willReturn(2);
      given(eventRepository.findByTaskIdAndStartingWithLineSanMaxOccurredOn(any(), any())).willReturn(Optional.empty());

      EngineInformationReceived result = cut.fillUpGameIfTooShort(event);

      assertThat(result, is(equalTo(event)));
      verify(eventRepository).findByTaskIdAndStartingWithLineSanMaxOccurredOn(any(), any());
    }
  }

  @Nested
  class DoesReplaceLine {

    private EngineInformationReceived event;
    private EngineInformationReceived storedEvent;
    private TaskId taskId;

    @BeforeEach
    void prepare() {
      taskId = ImmutableTaskId.of("task1");
      storedEvent = ImmutableEngineInformationReceived.builder()
          .calculatedPlies(5)
          .depth(19)
          .lineSan("2... Nf6 3. Nc3 Bb4 4. Qc2 g6")
          .multiPv(1)
          .nodes(1000)
          .nodesPerSecond(1000)
          .occurredOn(LocalDateTime.now(ZoneId.systemDefault()))
          .pgn("1.d4 d5 2.c4 Nf6 3. Nc3 Bb4 4. Qc2 g6*")
          .score(30)
          .taskId(taskId)
          .tbHits(0L)
          .time(234332L)
          .build();
      event = ImmutableEngineInformationReceived.builder()
          .calculatedPlies(2)
          .depth(20)
          .lineSan("2... Nf6 3. Nc3")
          .multiPv(1)
          .nodes(2000)
          .nodesPerSecond(1000)
          .occurredOn(LocalDateTime.now(ZoneId.systemDefault()))
          .pgn("1.d4 d5 2.c4 Nf6 3. Nc3*")
          .score(40)
          .taskId(taskId)
          .tbHits(0L)
          .time(234332L)
          .build();
    }

    @Test
    void replacesGameWithStoredValue() {
      given(eventRepository.findByTaskIdAndStartingWithLineSanMaxOccurredOn(taskId, event.lineSan()))
          .willReturn(Optional.of(storedEvent));

      EngineInformationReceived result = cut.fillUpGameIfTooShort(event);

      assertThat(result.calculatedPlies(), is(equalTo(storedEvent.calculatedPlies())));
      assertThat(result.lineSan(), is(equalTo(storedEvent.lineSan())));
      assertThat(result.pgn(), is(equalTo(storedEvent.pgn())));

      assertThat(result.depth(), is(equalTo(event.depth())));
      assertThat(result.score(), is(equalTo(event.score())));
    }

  }
}
