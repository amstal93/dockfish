package ce.chess.dockfish.domain.model.result;

import ce.chess.commontech.resource.jaxrs.Representation;
import ce.chess.dockfish.domain.event.EngineInformationReceived;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDateTime;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonDeserialize(as = ImmutableEngineInformation.class)
@JsonSerialize(as = ImmutableEngineInformation.class)
@Schema(name = "EngineInformation")
public interface EngineInformation {

  int multiPv();

  String lineSan();

  int score();

  String time();

  int depth();

  LocalDateTime occurredOn();

  static EngineInformation from(EngineInformationReceived event) {
    return ImmutableEngineInformation.builder()
        .multiPv(event.multiPv())
        .lineSan(event.lineSan())
        .score(event.score())
        .time(AnalysisTime.fromMilliSeconds(event.time()).formattedAsTime())
        .depth(event.depth())
        .occurredOn(event.occurredOn())
        .build();
  }
}
