package ce.chess.dockfish.domain.model.task;

import java.util.Optional;

public interface TaskRepository {

  void save(EngineAnalysisRequest task);

  Optional<EngineAnalysisRequest> findLatest();

  Optional<EngineAnalysisRequest> findByTaskId(TaskId taskId);

  boolean hasDuplicate(EngineAnalysisRequest engineTask);
}
