package ce.chess.dockfish.domain.event;

import ce.chess.dockfish.domain.model.task.TaskId;

import com.google.common.base.Strings;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.function.Predicate;
import org.immutables.value.Value;

@Value.Immutable
public interface EngineInformationReceived {

  static Predicate<EngineInformationReceived> matching(TaskId taskId) {
    return i -> i.taskId().matches(taskId);
  }

  TaskId taskId();

  int multiPv();

  int depth();

  long time();

  int score();

  long nodes();

  default long kiloNodes() {
    return nodes() / 1000;
  }

  long nodesPerSecond();

  default long kiloNodesPerSecond() {
    return nodesPerSecond() / 1000;
  }

  long tbHits();

  String pgn();

  String lineSan();

  int calculatedPlies();

  LocalDateTime occurredOn();

  Set<String> infoStrings();

  default boolean hasGame() {
    return !Strings.isNullOrEmpty(pgn());
  }
}
