package ce.chess.dockfish.domain.event;

import ce.chess.dockfish.domain.model.task.TaskId;

import org.immutables.value.Value;

@Value.Immutable
@Value.Style(allParameters = true)
public interface DepthReached {
  TaskId taskId();

  int depth();
}
