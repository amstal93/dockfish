package ce.chess.dockfish.domain.model.task;

import ce.chess.dockfish.application.command.DynamicPv;
import ce.chess.dockfish.application.command.EngineOption;
import ce.chess.dockfish.domain.model.result.ResultGame;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(allParameters = true,
    depluralize = true,
    jdkOnly = true)
public interface EngineAnalysisRequest {
  TaskId taskId();

  Optional<String> name();

  String engineProgramName();

  Optional<String> uciEngineName();

  String hostname();

  ResultGame startingPosition();

  Integer startingMoveNumber();

  Optional<String> startingFen();

  Integer initialPv();

  Optional<Integer> maxDepth();

  Optional<Duration> maxDuration();

  List<EngineOption> engineOptions();

  @Value.Default
  default boolean useSyzygyPath() {
    return false;
  }

  Optional<DynamicPv> dynamicPv();

  LocalDateTime created();

  default String startingPgn() {
    return startingPosition().pgn();
  }

  default Optional<LocalDateTime> estimatedCompletionTime() {
    return maxDuration().map(duration -> created().plus(duration));
  }

  default boolean isSameAs(EngineAnalysisRequest engineTask) {
    // equals ignoring taskId, uciEngineName, created date
    return Objects.equals(this.name(), engineTask.name())
        && Objects.equals(this.engineProgramName(), engineTask.engineProgramName())
        && Objects.equals(this.startingPosition(), engineTask.startingPosition())
        && Objects.equals(this.startingMoveNumber(), engineTask.startingMoveNumber())
        && Objects.equals(this.initialPv(), engineTask.initialPv())
        && Objects.equals(this.maxDepth(), engineTask.maxDepth())
        && Objects.equals(this.maxDuration(), engineTask.maxDuration())
        && Objects.equals(this.engineOptions(), engineTask.engineOptions())
        && Objects.equals(this.useSyzygyPath(), engineTask.useSyzygyPath())
        && Objects.equals(this.dynamicPv(), engineTask.dynamicPv());
  }
}
