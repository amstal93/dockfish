package ce.chess.dockfish.domain.event;

import ce.chess.dockfish.domain.model.task.TaskId;

import java.time.Instant;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(allParameters = true)
public interface AnalysisFinished {
  TaskId taskId();

  Instant occurredOn();

}
