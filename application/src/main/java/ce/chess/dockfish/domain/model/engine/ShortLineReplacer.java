package ce.chess.dockfish.domain.model.engine;

import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.event.ImmutableEngineInformationReceived;

import java.util.Objects;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class ShortLineReplacer {
  private static final Logger sLogger = LogManager.getLogger(ShortLineReplacer.class);

  private final EngineInformationReceivedRepository eventRepository;

  @Inject
  ShortLineReplacer(EngineInformationReceivedRepository eventRepository) {
    this.eventRepository = Objects.requireNonNull(eventRepository);
  }

  public EngineInformationReceived fillUpGameIfTooShort(EngineInformationReceived engineEvent) {
    if (engineEvent.hasGame() && engineEvent.calculatedPlies() < 3) {
      EngineInformationReceived normalizedEngineInformation =
          eventRepository.findByTaskIdAndStartingWithLineSanMaxOccurredOn(engineEvent.taskId(), engineEvent.lineSan())
              .map(previous -> copyWithGameFrom(engineEvent, previous))
              .orElse(engineEvent);

      if (!normalizedEngineInformation.equals(engineEvent)) {
        sLogger.info("Replacing short line [{}] with [{}]",
            engineEvent::lineSan, normalizedEngineInformation::lineSan);
      }
      return normalizedEngineInformation;
    } else {
      return engineEvent;
    }
  }

  private EngineInformationReceived copyWithGameFrom(EngineInformationReceived event,
      EngineInformationReceived previousEvent) {
    return ImmutableEngineInformationReceived.copyOf(event)
        .withPgn(previousEvent.pgn())
        .withLineSan(previousEvent.lineSan())
        .withCalculatedPlies(previousEvent.calculatedPlies());
  }

}
