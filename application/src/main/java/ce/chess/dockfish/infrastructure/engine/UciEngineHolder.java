package ce.chess.dockfish.infrastructure.engine;

import ce.chess.dockfish.application.command.EngineOption;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import raptor.engine.uci.UCIEngine;
import raptor.engine.uci.UCIOption;
import raptor.engine.uci.options.UCISpinner;

@Singleton
public class UciEngineHolder {

  private static final Logger sLogger = LogManager.getLogger(UciEngineHolder.class);

  private final EngineDirectoryConfiguration engineDirConfiguration;

  private final UCIEngine uciEngine;

  private final AtomicBoolean isReleasing;

  @Inject
  public UciEngineHolder(EngineDirectoryConfiguration engineDirConfiguration, UCIEngine uciEngine) {
    this.engineDirConfiguration = engineDirConfiguration;
    this.uciEngine = uciEngine;
    this.isReleasing = new AtomicBoolean(false);
  }

  public UCIEngine getEngine() {
    return uciEngine;
  }

  public UCIEngine connect(String engineName) {
    return connect(engineName, 0, Collections.emptyList());
  }

  public UCIEngine connect(String engineName, int initialPVs, List<EngineOption> options) {
    if (uciEngine.isProcessingGo()) {
      throw new IllegalStateException("UCIEngine is already active");
    }

    disconnect();

    uciEngine.setProcessPath(engineDirConfiguration.validatedProcessPathFor(engineName));

    sLogger.info("Connecting to {}", uciEngine.getProcessPath());
    if (!uciEngine.connect()) {
      throw new IllegalStateException("Failed to connect engine " + uciEngine.getProcessPath());
    }

    setMultiPv(initialPVs);
    options.forEach(option -> setOption(option.name(), option.value()));
    Arrays.stream(uciEngine.getOptionNames())
        .map(uciEngine::getOption)
        .sorted(Comparator.comparing(UCIOption::getName))
        .forEach(sLogger::info);
    return uciEngine;
  }

  public void disconnect() {
    if (!isConnected()) {
      return;
    }
    if (isReleasing.compareAndSet(false, true)) {
      sLogger.info("Disconnecting from {}", uciEngine.getProcessPath());
      try {
        uciEngine.quit();
      } finally {
        isReleasing.compareAndSet(true, false);
      }
    }
  }

  public boolean isConnected() {
    return uciEngine.isConnected();
  }

  private void setMultiPv(int initialPVs) {
    UCIOption multiPv = new UCISpinner();
    multiPv.setName("MultiPV");
    multiPv.setValue(Integer.toString(initialPVs));
    uciEngine.setOption(multiPv);
  }

  private void setOption(String optionKey, String optionValue) {
    if (uciEngine.hasOption(optionKey)) {
      UCIOption uciOption = uciEngine.getOption(optionKey);
      uciOption.setName(optionKey);
      uciOption.setValue(optionValue);
      uciEngine.setOption(uciOption);
    }
  }

}
