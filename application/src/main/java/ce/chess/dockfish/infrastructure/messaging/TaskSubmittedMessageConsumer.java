package ce.chess.dockfish.infrastructure.messaging;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.Message;
import ce.chess.dockfish.application.InfiniteAnalysisService;
import ce.chess.dockfish.application.command.SubmitTaskCommand;
import ce.chess.dockfish.domain.model.task.TaskId;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.Uninterruptibles;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
public class TaskSubmittedMessageConsumer extends AbstractMessageListener {
  private static final Logger sLogger = LogManager.getLogger(TaskSubmittedMessageConsumer.class);

  private static final String QUEUE_NAME_TASKS_ENV_VARIABLE = "queue_name_tasks";
  private static final String ROUTING_KEY_ENV_VARIABLE = "routing_key_task";
  private static final String CONSUMER_ARGUMENTS_ENV_VARIABLE = "consumer_arguments_task";

  private static final String QUEUE_TASKS_DEFAULT = "dockfish.tasks.default";
  private static final String EXCHANGE_TASK = "task.submitted";

  @Inject
  ObjectMapper objectMapper;

  @Inject
  Config config;

  @Inject
  InfiniteAnalysisService service;

  @Override
  public void handleMessage(Message message) {
    sLogger.info(" [x] Received message with routing key {}: '{}'", message.getRoutingKey(), message.getBodyAsString());

    SubmitTaskCommand command = readSubmitTaskCommand(message);

    // be gentle to StaticEvaluationRequestsConsumer
    Long initialDelay = config.getOptionalValue("task_consumer_delay_seconds", Long.class).orElse(3L);
    Uninterruptibles.sleepUninterruptibly(initialDelay, TimeUnit.SECONDS);

    TaskId taskId = service.startSync(command)
        .orElseThrow(() -> new IllegalStateException("No Task Id. Engine not running"));
    sLogger.info("Finished calculation of taskID {}", taskId);
  }

  private SubmitTaskCommand readSubmitTaskCommand(Message message) {
    try {
      return objectMapper.readValue(message.getBody(), SubmitTaskCommand.class);
    } catch (IOException ex) {
      sLogger.error("Cannot deserialize message content: [{}]; reason: {}",
          message.getBodyAsString(), ex.getMessage());
      throw new UncheckedIOException(ex);
    }
  }

  @Override
  public String getExchangeName() {
    return EXCHANGE_TASK;
  }

  @Override
  public String getExchangeType() {
    return "TOPIC";
  }

  @Override
  public List<String> getQueueNames() {
    String csvQueues = config.getOptionalValue(QUEUE_NAME_TASKS_ENV_VARIABLE, String.class)
        .orElse(QUEUE_TASKS_DEFAULT);
    return List.of(csvQueues.split("\\s*;\\s*"));
  }

  @Override
  public List<List<String>> getRoutingKeysBinding() {
    String csvValue = config.getOptionalValue(ROUTING_KEY_ENV_VARIABLE, String.class)
        .orElse("#");
    return Stream.of(csvValue.split("\\s*;\\s*"))
        .map(s -> s.split(","))
        .map(List::of)
        .toList();
  }

  @Override
  public Map<String, Object> getConsumerArguments() {
    Map<String, Object> result = new ConcurrentHashMap<>();
    List<String> keyValues = config.getOptionalValue(CONSUMER_ARGUMENTS_ENV_VARIABLE, String.class)
        .stream()
        .map(s -> s.split(","))
        .flatMap(Arrays::stream)
        .toList();
    IntStream.range(0, keyValues.size() / 2)
        .map(i -> i * 2)
        .forEach(i -> {
          String key = keyValues.get(i);
          String value = keyValues.get(i + 1);
          result.put(key, value.matches("-?\\d+") ? Integer.valueOf(value) : value);
        });
    return result;

  }

  @Override
  public boolean isAutoAck() {
    return false;
  }

  @Override
  public int getPrefetchLimit() {
    return 1;
  }

}
