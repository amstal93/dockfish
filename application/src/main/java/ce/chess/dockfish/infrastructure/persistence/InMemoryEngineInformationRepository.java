package ce.chess.dockfish.infrastructure.persistence;

import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.model.engine.EngineInformationReceivedRepository;
import ce.chess.dockfish.domain.model.task.TaskId;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Singleton;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;

@Singleton
public class InMemoryEngineInformationRepository implements EngineInformationReceivedRepository {

  private final Cache<EngineInformationReceived, String> events =
      CacheBuilder.newBuilder().maximumSize(10_000).build();

  @Gauge(name = "guava_cache_size", absolute = true, unit = MetricUnits.NONE,
      tags = "cache=InMemoryEngineInformationRepository")
  public long getCacheSize() {
    return events.size();
  }

  @Override
  public void save(EngineInformationReceived event) {
    refreshInCache(event);
  }

  @Override
  public Collection<EngineInformationReceived> findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(TaskId taskId) {
    Map<Integer, Optional<EngineInformationReceived>> deepestEventsByPv = getEvents().stream()
        .filter(EngineInformationReceived.matching(taskId))
        .filter(EngineInformationReceived::hasGame)
        .collect(
            Collectors.groupingBy(
                EngineInformationReceived::multiPv,
                Collectors.maxBy(
                    Comparator.comparingInt(EngineInformationReceived::depth)
                        .thenComparing(EngineInformationReceived::occurredOn))));
    return deepestEventsByPv.values().stream()
        .flatMap(Optional::stream)
        .map(this::refreshInCache)
        .collect(Collectors.toList());
  }

  @Override
  public Optional<EngineInformationReceived> findByTaskIdMaxOccurredOn(TaskId taskId) {
    return getEvents().stream()
        .filter(EngineInformationReceived.matching(taskId))
        .max(Comparator.comparing(EngineInformationReceived::occurredOn));
  }

  @Override
  public Optional<EngineInformationReceived> findByTaskIdAndStartingWithLineSanMaxOccurredOn(TaskId taskId,
                                                                                             String lineSan) {
    return getEvents().stream()
        .filter(EngineInformationReceived.matching(taskId))
        .filter(EngineInformationReceived::hasGame)
        .filter(e -> e.lineSan().startsWith(lineSan))
        .max(Comparator.comparing(EngineInformationReceived::occurredOn));
  }

  private EngineInformationReceived refreshInCache(EngineInformationReceived engineInformationReceived) {
    events.put(engineInformationReceived, "");
    return engineInformationReceived;
  }

  private Set<EngineInformationReceived> getEvents() {
    return events.asMap().keySet();
  }

}
