package ce.chess.dockfish.infrastructure.messaging;

import ce.chess.commontech.messaging.AbstractMessagePublisher;
import ce.chess.dockfish.application.EvaluationMessageService;
import ce.chess.dockfish.domain.event.AnalysisFinished;
import ce.chess.dockfish.domain.model.result.EvaluationMessage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import java.io.IOException;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class FinalResultPublisher extends AbstractMessagePublisher {
  private static final Logger LOGGER = LogManager.getLogger(FinalResultPublisher.class);

  private static final String EXCHANGE_RESULTS = "evaluation.created";
  private static final String QUEUE_RESULTS = "dockfish.evaluations";

  @Inject
  EvaluationMessageService resultService;

  @Inject
  ObjectMapper objectMapper;

  @Inject
  Event<PublishFailed> publishFailedEvent;

  public void publishFinalEvaluation(@Observes AnalysisFinished event) {
    resultService.getLastEvaluationMessage(event.taskId())
        .ifPresentOrElse(evaluation -> sendMessage(event, evaluation),
            () -> logNoEvaluationFound(event));
  }

  private void sendMessage(AnalysisFinished event, EvaluationMessage evaluation) {
    String bodyContent = "";
    try {
      bodyContent = Strings.nullToEmpty(objectMapper.writeValueAsString(evaluation));
      LOGGER.info(" [x] Publishing '{}'", bodyContent);
      publishAndClose(bodyContent);
    } catch (IOException ex) {
      LOGGER.error("Failed to publish final result: {}", event, ex);
      publishFailedEvent.fire(ImmutablePublishFailed.of(getExchangeName(), bodyContent));
    }
  }

  private void logNoEvaluationFound(AnalysisFinished event) {
    LOGGER.warn("No evaluation message found for taskId {}", event::taskId);
  }

  @Override
  public String getExchangeName() {
    return EXCHANGE_RESULTS;
  }

  @Override
  public String getQueueName() {
    return QUEUE_RESULTS;
  }

}
