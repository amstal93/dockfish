package ce.chess.dockfish.infrastructure.messaging;

import org.immutables.value.Value;

@Value.Immutable
@Value.Style(allParameters = true)
public interface PublishFailed {
  String exchangeName();

  String message();
}
