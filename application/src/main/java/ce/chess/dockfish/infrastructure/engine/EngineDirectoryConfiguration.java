package ce.chess.dockfish.infrastructure.engine;

import io.quarkus.runtime.StartupEvent;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
public class EngineDirectoryConfiguration {
  private static final Logger sLogger = LogManager.getLogger(EngineDirectoryConfiguration.class);

  private static final String ENGINE_DIR_PROPERTY = "engine_directory";
  private static final String DEFAULT_ENGINE_DIR = "/engines";
  private static final String ADDITIONAL_ENGINE_DIR_PROPERTY = "additional_engine_directory";

  @Inject
  Config config;

  public String validatedProcessPathFor(String engineName) {
    if (engineName.startsWith("..") || engineName.startsWith("/") || engineName.startsWith("\\")) {
      throw new IllegalArgumentException("Illegal engine program name: " + engineName);
    }
    return Optional.ofNullable(enginePathsByEngineName().get(engineName))
        .orElseThrow(() -> new IllegalArgumentException("Engine not found: " + engineName));
  }

  public Set<String> listEngineNames() {
    return enginePathsByEngineName().keySet();
  }

  public void contextInitialized(@Observes StartupEvent sce) {
    sLogger.info("Available engines: {}", listEngineNames());
  }

  private Map<String, String> enginePathsByEngineName() {
    Map<String, String> result = new ConcurrentHashMap<>();
    appendFilesToMap(getEngineDirectory(), result);
    getAdditionalEngineDirectory().ifPresent(
        directory -> appendFilesToMap(directory, result));
    return result;
  }

  private void appendFilesToMap(String directory, Map<String, String> result) {

    Path dir = Paths.get(directory);
    if (Files.exists(dir)) {
      try (Stream<Path> stream = Files.list(dir)) {
        stream
            .filter(Files::isRegularFile)
            .filter(Files::isExecutable)
            .forEach(p -> result.put(p.getFileName().toString(), p.toString()));
      } catch (IOException ioe) {
        throw new UncheckedIOException(ioe);
      }
    }
  }

  String getEngineDirectory() {
    return config.getOptionalValue(ENGINE_DIR_PROPERTY, String.class)
        .orElse(DEFAULT_ENGINE_DIR);
  }

  Optional<String> getAdditionalEngineDirectory() {
    return config.getOptionalValue(ADDITIONAL_ENGINE_DIR_PROPERTY, String.class);
  }
}
