package ce.chess.dockfish.infrastructure.messaging;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.Message;
import ce.chess.dockfish.application.StaticEvaluationService;
import ce.chess.dockfish.domain.model.staticevaluation.StaticEvaluationRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class StaticEvaluationRequestConsumer extends AbstractMessageListener {
  private static final Logger sLogger = LogManager.getLogger(StaticEvaluationRequestConsumer.class);

  private static final String EXCHANGE_EVAL_TASK = "staticEvaluationRequest.submitted";
  private static final String QUEUE_EVAL_TASKS = "dockfish.staticEvaluationRequests.default";

  @Inject
  ObjectMapper objectMapper;

  @Inject
  StaticEvaluationService service;

  @Override
  public void handleMessage(Message message) {
    sLogger.info(" [x] Received '{}'", message.getBodyAsString());

    StaticEvaluationRequest command = readStaticEvaluationRequest(message);
    service.createAndPublishEvaluation(command);
  }

  private StaticEvaluationRequest readStaticEvaluationRequest(Message message) {
    try {
      return objectMapper.readValue(message.getBody(), StaticEvaluationRequest.class);
    } catch (IOException ex) {
      sLogger.error("Cannot deserialize message content: [{}]; reason: {}",
          message.getBodyAsString(), ex.getMessage());
      throw new UncheckedIOException(ex);
    }
  }

  @Override
  public String getExchangeName() {
    return EXCHANGE_EVAL_TASK;
  }

  @Override
  public String getExchangeType() {
    return "TOPIC";
  }

  @Override
  public List<String> getQueueNames() {
    return List.of(QUEUE_EVAL_TASKS);
  }

  @Override
  public boolean isAutoAck() {
    return false;
  }

  @Override
  public int getPrefetchLimit() {
    return 1;
  }
}
