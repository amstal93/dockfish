package ce.chess.dockfish.infrastructure.persistence;

import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.model.task.TaskRepository;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.Comparator;
import java.util.Optional;
import javax.inject.Singleton;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;

@Singleton
public class InMemoryTaskRepository implements TaskRepository {

  private final Cache<TaskId, EngineAnalysisRequest> tasks = CacheBuilder.newBuilder().maximumSize(100).build();

  @Gauge(name = "guava_cache_size", absolute = true, unit = MetricUnits.NONE,
      tags = "cache=InMemoryTaskRepository")
  public long getCacheSize() {
    return tasks.size();
  }

  @Override
  public void save(EngineAnalysisRequest task) {
    tasks.put(task.taskId(), task);
  }

  @Override
  public Optional<EngineAnalysisRequest> findLatest() {
    return tasks.asMap().values().stream()
        .max(Comparator.comparing(EngineAnalysisRequest::created));
  }

  @Override
  public Optional<EngineAnalysisRequest> findByTaskId(TaskId taskId) {
    return tasks.asMap().keySet().stream()
        .filter(key -> key.matches(taskId))
        .findFirst()
        .map(tasks::getIfPresent);
  }

  @Override
  public boolean hasDuplicate(EngineAnalysisRequest engineTask) {
    return tasks.asMap().values().stream().anyMatch(engineTask::isSameAs);
  }

}
