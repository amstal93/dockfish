package ce.chess.dockfish.application.command;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableEngineOption.class)
@JsonDeserialize(as = ImmutableEngineOption.class)
public interface EngineOption {

  @Schema(example = "Hash")
  String name();

  @Schema(example = "1024")
  String value();
}
