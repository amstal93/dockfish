package ce.chess.dockfish.application;

import ce.chess.dockfish.domain.event.DepthReached;
import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.event.ImmutableDepthReached;
import ce.chess.dockfish.domain.model.engine.EngineInformationReceivedRepository;
import ce.chess.dockfish.domain.model.engine.ShortLineReplacer;
import ce.chess.dockfish.domain.model.result.AnalysisTime;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationRepository;
import ce.chess.dockfish.domain.model.result.ImmutableEvaluation;
import ce.chess.dockfish.domain.model.result.ImmutableUciState;
import ce.chess.dockfish.domain.model.result.ImmutableVariation;
import ce.chess.dockfish.domain.model.result.ResultGame;
import ce.chess.dockfish.domain.model.result.Score;
import ce.chess.dockfish.domain.model.result.Variation;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class EngineInformationReceivedHandler {
  private static final Logger sLogger = LogManager.getLogger(EngineInformationReceivedHandler.class);
  private static final int VERBOSE_LOG_AFTER_MINUTES = 30;

  private final EngineInformationReceivedRepository eventRepository;
  private final EvaluationRepository evaluationRepository;
  private final Event<DepthReached> newDepthEvent;
  private final ShortLineReplacer shortLineFixer;
  private int lastSentDepth;

  @Inject
  EngineInformationReceivedHandler(EngineInformationReceivedRepository eventRepository,
                                   EvaluationRepository evaluationRepository,
                                   Event<DepthReached> newDepthEvent, ShortLineReplacer shortLineFixer) {
    this.eventRepository = Objects.requireNonNull(eventRepository);
    this.evaluationRepository = Objects.requireNonNull(evaluationRepository);
    this.newDepthEvent = Objects.requireNonNull(newDepthEvent);
    this.shortLineFixer = Objects.requireNonNull(shortLineFixer);
  }

  public void receive(@Observes EngineInformationReceived engineInformation) {
    EngineInformationReceived engineInformationNormalized = shortLineFixer.fillUpGameIfTooShort(engineInformation);

    eventRepository.save(engineInformationNormalized);

    if (engineInformationNormalized.hasGame()) {
      if (sLogger.isDebugEnabled()
          || Duration.ofMillis(engineInformation.time()).toMinutes() > VERBOSE_LOG_AFTER_MINUTES) {
        sLogger.info("pv-{} {}[{},{}d]",
            engineInformationNormalized.multiPv(),
            Score.fromCentiPawns(engineInformationNormalized.score()),
            String.format("%.45s", engineInformationNormalized.lineSan()),
            engineInformationNormalized.depth());
      }
      createAndPublishEvaluationFor(engineInformationNormalized);
    }
  }

  private void createAndPublishEvaluationFor(EngineInformationReceived event) {
    Collection<EngineInformationReceived> deepestEvents =
        eventRepository.findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(event.taskId());
    Evaluation evaluation = createEvaluation(event, deepestEvents);

    if (evaluation.hasAllVariationsOfSameDepth()) {
      evaluationRepository.save(evaluation);
      if (lastSentDepth != evaluation.maxDepth()) {
        newDepthEvent.fire(ImmutableDepthReached.of(event.taskId(), evaluation.maxDepth()));
        lastSentDepth = evaluation.maxDepth();
      }
    }
  }

  private Evaluation createEvaluation(EngineInformationReceived event,
                                      Collection<EngineInformationReceived> deepestEvents) {
    return ImmutableEvaluation.builder()
        .taskId(event.taskId())
        .created(event.occurredOn())
        .variations(createVariations(deepestEvents))
        .uciState(
            ImmutableUciState.of(event.kiloNodes(), event.kiloNodesPerSecond(), event.tbHits(), event.infoStrings()))
        .build();
  }

  private List<Variation> createVariations(Collection<EngineInformationReceived> deepestEvents) {
    return deepestEvents.stream()
        .map(this::createVariation)
        .toList();
  }

  private Variation createVariation(EngineInformationReceived event) {
    return ImmutableVariation.of(event.multiPv(),
        event.lineSan(),
        Score.fromCentiPawns(event.score()),
        event.depth(),
        AnalysisTime.fromMilliSeconds(event.time()),
        ResultGame.fromPgn(event.pgn()).notation());
  }

}
