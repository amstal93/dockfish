package ce.chess.dockfish.application.command;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
public class UciOptionsConfiguration {

  private static final String UCI_OPTION_PREFIX = "uci_option_";

  @Inject
  Config config;

  public List<EngineOption> getLocalEngineOptions() {
    return StreamSupport.stream(config.getPropertyNames().spliterator(), false)
        .filter(property -> property.startsWith(UCI_OPTION_PREFIX))
        .map(this::createEngineOption)
        .collect(Collectors.toList());
  }

  private ImmutableEngineOption createEngineOption(String property) {
    return ImmutableEngineOption.builder()
        .name(property.replace(UCI_OPTION_PREFIX, "").replace("_", " "))
        .value(config.getValue(property, String.class))
        .build();
  }

}
