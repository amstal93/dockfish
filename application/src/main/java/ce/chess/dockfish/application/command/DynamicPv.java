package ce.chess.dockfish.application.command;

import ce.chess.commontech.resource.jaxrs.Representation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.immutables.value.Value;

@Value.Immutable
@Representation
@JsonSerialize(as = ImmutableDynamicPv.class)
@JsonDeserialize(as = ImmutableDynamicPv.class)
public interface DynamicPv {
  @Schema(example = "30")
  int requiredDepth();

  @Schema(example = "20")
  int cutOffCentiPawns();

  @Schema(example = "2")
  default int keepMinPv() {
    return 1;
  }
}
