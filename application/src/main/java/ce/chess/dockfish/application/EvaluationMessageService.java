package ce.chess.dockfish.application;

import ce.chess.dockfish.domain.event.EngineInformationReceived;
import ce.chess.dockfish.domain.model.engine.EngineInformationReceivedRepository;
import ce.chess.dockfish.domain.model.result.EngineInformation;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.EvaluationMessage;
import ce.chess.dockfish.domain.model.result.EvaluationRepository;
import ce.chess.dockfish.domain.model.result.ImmutableEvaluationMessage;
import ce.chess.dockfish.domain.model.result.JobStatus;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.domain.model.task.TaskId;
import ce.chess.dockfish.domain.model.task.TaskRepository;
import ce.chess.dockfish.infrastructure.engine.EngineController;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.config.Config;

@ApplicationScoped
public class EvaluationMessageService {
  @Inject
  TaskRepository taskRepository;

  @Inject
  EvaluationRepository evaluationRepository;

  @Inject
  EngineInformationReceivedRepository engineInformationRepository;

  @Inject
  EngineController engineController;

  @Inject
  Config config;

  public Optional<EvaluationMessage> getLastEvaluationMessage() {
    return taskRepository.findLatest()
        .map(EngineAnalysisRequest::taskId)
        .flatMap(this::getLastEvaluationMessage);
  }

  public Optional<EvaluationMessage> getLastEvaluationMessage(TaskId taskId) {
    return evaluationRepository.findByTaskIdMaxCreated(taskId)
        .map(latestEvaluation -> {
          EngineAnalysisRequest task = taskRepository.findByTaskId(taskId)
              .orElseThrow(() -> new IllegalArgumentException("Task not found: " + taskId));

          LocalDateTime lastAlive = engineInformationRepository.findByTaskIdMaxOccurredOn(taskId)
              .map(EngineInformationReceived::occurredOn)
              .orElseGet(latestEvaluation::created);

          List<EngineInformation> latestEvents =
              engineInformationRepository.findByTaskIdGroupedByMultiPvMaxDepthAndMaxOccurredOn(taskId).stream()
                  .map(EngineInformation::from)
                  .toList();

          List<String> history = evaluationRepository.findByTaskId(taskId).stream()
              .sorted(Comparator.comparingInt(Evaluation::maxDepth).reversed())
              .limit(20)
              .filter(evaluation -> evaluation.maxDepth() > 15)
              .map(Evaluation::shortForm)
              .toList();

          return ImmutableEvaluationMessage.builder()
              .taskName(task.name())
              .analysedPgn(task.startingPgn())
              .analysedFen(task.startingFen().orElse(""))
              .uciEngineName(task.uciEngineName().orElseGet(task::engineProgramName))
              .taskDepth(task.maxDepth())
              .taskDuration(task.maxDuration())
              .hostname(config.getOptionalValue("hostname", String.class).orElse("hostname"))
              .status(getJobStatus(taskId))
              .evaluation(latestEvaluation)
              .taskStarted(task.created())
              .lastAlive(lastAlive)
              .latestEvents(latestEvents)
              .history(history)
              .build();
        });
  }

  public List<TaskId> getAllTaskIds() {
    return evaluationRepository.listTaskIds();
  }

  public List<Evaluation> getAllEvaluations() {
    return taskRepository.findLatest()
        .map(EngineAnalysisRequest::taskId)
        .map(this::getAllEvaluations)
        .orElseGet(List::of);
  }

  public List<Evaluation> getAllEvaluations(TaskId taskId) {
    return evaluationRepository.findByTaskId(taskId);
  }

  private JobStatus getJobStatus(TaskId taskId) {
    if (engineController.uciEngineIsRunning(taskId)) {
      return JobStatus.ACTIVE;
    } else {
      return JobStatus.NOT_ACTIVE;
    }
  }

}
