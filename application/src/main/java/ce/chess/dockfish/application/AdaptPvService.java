package ce.chess.dockfish.application;

import static java.lang.Math.max;

import ce.chess.dockfish.application.command.DynamicPv;
import ce.chess.dockfish.domain.model.result.Evaluation;
import ce.chess.dockfish.domain.model.result.Variation;
import ce.chess.dockfish.domain.model.task.EngineAnalysisRequest;
import ce.chess.dockfish.infrastructure.engine.EngineController;

import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class AdaptPvService {
  private static final Logger sLogger = LogManager.getLogger(AdaptPvService.class);

  @Inject
  EngineController engineController;

  public void adaptPv(Evaluation evaluation, EngineAnalysisRequest task) {
    task.dynamicPv()
        .filter(taskConfiguration -> taskConfiguration.requiredDepth() <= evaluation.maxDepth())
        .ifPresent(taskConfiguration -> apply(taskConfiguration, evaluation));
  }

  private void apply(DynamicPv taskConfiguration, Evaluation evaluation) {
    int keepAtLeastPvFromConfiguration = taskConfiguration.keepMinPv();
    int currentPv = evaluation.sizeOfCurrentVariations();
    if (currentPv > keepAtLeastPvFromConfiguration) {
      int cutoffConfiguration = taskConfiguration.cutOffCentiPawns();
      int newPv = max(evaluation.determineNumberOfGoodPv(cutoffConfiguration), keepAtLeastPvFromConfiguration);
      if (newPv < currentPv) {
        reducePv(newPv, evaluation);
      }
    }
  }

  private void reducePv(int newPv, Evaluation evaluation) {
    evaluation.variations().stream()
        .skip(newPv)
        .forEach(this::logVariation);
    sLogger.info("Keep running with {}", () -> evaluation.variations().get(0).shortRepresentation());

    engineController.reducePvTo(newPv);
  }

  private void logVariation(Variation variation) {
    sLogger.info("Stop analysis on {} with last variation: {}", variation::shortRepresentation, variation::moves);
  }

}
