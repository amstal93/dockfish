FROM eclipse-temurin:17.0.3_7-jdk-focal AS jre-build
RUN jlink  \
  --add-modules java.base\
,java.datatransfer\
,java.instrument\
,java.logging\
,java.management\
,java.naming\
,java.security.sasl\
,java.sql\
,java.xml\
,jdk.management\
,jdk.unsupported \
  --strip-java-debug-attributes \
  --no-man-pages \
  --no-header-files \
  --compress=2 \
  --bind-services \
  --output /jre
RUN cd /jre/bin && rm -f jar jarsigner javac javadoc javap \
    jcmd jconsole jdb jdeprscan jdeps jfr jhsdb jimage \
    jinfo jlink jmap jmod jpackage jps jrunscript jstack jstat \
    rmiregistry serialver

FROM ubuntu:jammy
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends wget \
    && rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME=/opt/java/openjdk
ENV PATH "${JAVA_HOME}/bin:${PATH}"
COPY --from=jre-build /jre $JAVA_HOME
RUN java -version

EXPOSE 8080/tcp
HEALTHCHECK --interval=2m --timeout=20s --retries=2 CMD wget --quiet --tries=1 --spider http://localhost:8080/q/health || exit 1
COPY maven /
RUN chmod +x engines/komodo engines/stockfish
RUN echo uci | engines/komodo
RUN echo uci | engines/stockfish
ENTRYPOINT [ -d "/run/secrets" ] && \
 export $( grep -v '^#' /run/secrets/* | sed s/":"/"="/ |sed s/"\/run\/secrets\/"//); \
 exec java -Xms32m -Xmx64m -XX:+ExitOnOutOfMemoryError -jar dockfish-web-runner.jar
