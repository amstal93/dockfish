package ce.chess.integration.model;

import java.util.Set;

public class UciState {
  public long kiloNodes;

  public long kiloNodesPerSecond;

  public long tbHits;

  public Set<String> infoStrings;

}
