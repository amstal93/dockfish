package ce.chess.integration.run;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"classpath:specification"},
    glue = {"ce.chess"},
    publish = true,
    plugin = {"pretty"})
public class CucumberIT {

}
