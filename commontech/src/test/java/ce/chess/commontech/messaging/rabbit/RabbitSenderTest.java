package ce.chess.commontech.messaging.rabbit;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import ce.chess.commontech.messaging.Message;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RabbitSenderTest {
  @Mock
  private SingleConnectionFactory connectionFactory;

  @Mock
  private Connection connection;

  @Mock
  private Channel channel;

  @InjectMocks
  private RabbitSender cut;

  private Message message;

  @BeforeEach
  void setUp() throws IOException, TimeoutException {
    lenient().when(connectionFactory.newConnection()).thenReturn(connection);
    lenient().when(connection.createChannel()).thenReturn(channel);
    lenient().when(channel.isOpen()).thenReturn(true);

    message = new Message(MessageProperties.BASIC)
        .withExchange("exchange")
        .withRoutingKey("routing")
        .withBody(new byte[]{42});
  }

  @Test
  void doesPublishToRabbitChannel() throws IOException, TimeoutException {

    cut.publish(message);

    InOrder inOrder = Mockito.inOrder(connectionFactory, connection, channel);
    inOrder.verify(connectionFactory).newConnection();
    inOrder.verify(connection).createChannel();
    inOrder.verify(channel).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
  }

  @Nested
  class WhenRabbitPublishFails {

    @Nested
    class AndItFailsOnlyOnce {
      @BeforeEach
      void setup() throws IOException {
        doThrow(IOException.class).doNothing()
            .when(channel).basicPublish(any(), any(), any(), eq(new byte[]{42}));
      }

      @Test
      void thenDoesReopenChannelAndRetry() throws Exception {
        cut.publish(message);

        InOrder inOrder = Mockito.inOrder(connectionFactory, connection, channel);
        inOrder.verify(connectionFactory).newConnection();
        inOrder.verify(connection).createChannel();
        inOrder.verify(channel).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
        inOrder.verify(channel).close();
        inOrder.verify(connection).createChannel();
        inOrder.verify(channel).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
        inOrder.verifyNoMoreInteractions();
      }

      @Test
      void whenReopenFailsThenStillTryAgain() throws Exception {
        doThrow(IOException.class).doNothing().when(channel).close();

        cut.publish(message);

        InOrder inOrder = Mockito.inOrder(connectionFactory, connection, channel);
        inOrder.verify(connectionFactory).newConnection();
        inOrder.verify(connection).createChannel();
        inOrder.verify(channel).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
        inOrder.verify(channel).close();
        inOrder.verify(connection).createChannel();
        inOrder.verify(channel).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
        inOrder.verifyNoMoreInteractions();
      }
    }

    @Nested
    class AndItFailsPermanently {
      @BeforeEach
      void setup() throws IOException {
        doThrow(IOException.class)
            .when(channel).basicPublish(any(), any(), any(), eq(new byte[]{42}));
      }

      @Test
      void thenDoesRetryTwiceThenThrowsIoException() throws Exception {
        assertThrows(IOException.class, () -> cut.publish(message));
        verify(connection, times(3)).createChannel();
        verify(channel, times(3)).basicPublish("exchange", "routing", MessageProperties.BASIC, new byte[]{42});
        verify(channel, times(3)).close();
      }
    }
  }

  @Test
  void doesCloseRabbitChannel() throws IOException, TimeoutException {

    try (RabbitSender cut = new RabbitSender(connectionFactory)) {
      cut.publish(mock(Message.class));
    }

    verify(channel).close();
  }

  @Test
  void whenClosingThenThrowsTimeoutExceptionAsIoException() throws IOException, TimeoutException {
    doThrow(TimeoutException.class).when(channel).close();
    assertThrows(IOException.class, () -> {
      try (RabbitSender cut = new RabbitSender(connectionFactory)) {
        cut.publish(mock(Message.class));
      }
    });
    verify(channel).close();
  }

  @Test
  void doesNotCloseAClosedChannel() throws IOException, TimeoutException {
    cut.publish(message); // make sure channel has been created

    given(channel.isOpen()).willReturn(false);
    cut.close();
    verify(channel, never()).close();
  }

}
