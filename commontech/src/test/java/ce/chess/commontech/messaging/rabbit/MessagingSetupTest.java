package ce.chess.commontech.messaging.rabbit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.AbstractMessagePublisher;

import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.Priority;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import nl.altindag.log.LogCaptor;
import org.awaitility.Awaitility;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith({MockitoExtension.class, WeldJunit5Extension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class MessagingSetupTest {
  private static LogCaptor logCaptor;

  @BeforeAll
  public static void setupLogCaptor() {
    logCaptor = LogCaptor.forClass(MessagingSetup.class);
  }

  @AfterEach
  public void clearLogCaptor() {
    logCaptor.clearLogs();
  }

  @AfterAll
  public static void closeLogCaptor() {
    logCaptor.resetLogLevel();
    logCaptor.close();
  }

  @WeldSetup
  public WeldInitiator weld = WeldInitiator.from(MessagingSetup.class, MockProducer.class).build();

  private static final int MAXIMUM_TIMEOUT_MILLIS = 10000;

  @Priority(1)
  static class MockProducer {
    @Produces
    @Alternative
    static SingleConnectionFactory connectionFactory = mock(SingleConnectionFactory.class);

    @Produces
    @Alternative
    static AbstractMessageListener listenerOfNamedQueue = mock(AbstractMessageListener.class, CALLS_REAL_METHODS);

    @Produces
    @Alternative
    static AbstractMessageListener listenerOfUnnamedQueue = mock(AbstractMessageListener.class, CALLS_REAL_METHODS);

    @Produces
    @Alternative
    static AbstractMessagePublisher publisherWithGivenQueue = mock(AbstractMessagePublisher.class);

    @Produces
    @Alternative
    static AbstractMessagePublisher publisherWithoutQueue = mock(AbstractMessagePublisher.class);
  }

  @Mock
  private Connection connection;

  @Mock
  private Channel channel;

  @Mock
  private DeclareOk declareOk;

  @Inject
  MessagingSetup cut;

  @BeforeEach
  void setUp() throws IOException, TimeoutException {
    Mockito.reset(MockProducer.connectionFactory);
    given(MockProducer.connectionFactory.newConnection()).willReturn(connection);
    given(connection.createChannel()).willReturn(channel);
    given(connection.getPort()).willReturn(1142);
    given(channel.queueDeclare(anyString(), eq(false), eq(false), eq(false), eq(Collections.emptyMap())))
        .willReturn(declareOk);
    given(channel.queueDeclare()).willReturn(declareOk);

    given(MockProducer.listenerOfNamedQueue.getExchangeName()).willReturn("ex1");
    given(MockProducer.listenerOfNamedQueue.getBuiltinExchangeType()).willReturn(BuiltinExchangeType.FANOUT);
    given(MockProducer.listenerOfNamedQueue.getQueueNames()).willReturn(List.of("qu1"));
    given(MockProducer.listenerOfNamedQueue.getRoutingKeysBinding())
        .willReturn(List.of(List.of("routing.key.1", "routing.key.2")));
    given(MockProducer.listenerOfNamedQueue.isAutoAck()).willReturn(true);
    given(MockProducer.listenerOfNamedQueue.getPrefetchLimit()).willReturn(1);

    given(MockProducer.listenerOfUnnamedQueue.getExchangeName()).willReturn("ex2");
    given(MockProducer.listenerOfUnnamedQueue.getBuiltinExchangeType()).willReturn(BuiltinExchangeType.TOPIC);
    given(MockProducer.listenerOfUnnamedQueue.getQueueNames()).willReturn(List.of(""));
    given(MockProducer.listenerOfUnnamedQueue.getRoutingKeysBinding()).willReturn(List.of(List.of("")));
    given(MockProducer.listenerOfUnnamedQueue.isAutoAck()).willReturn(true);
    given(MockProducer.listenerOfUnnamedQueue.getPrefetchLimit()).willReturn(2);

    given(MockProducer.publisherWithGivenQueue.getExchangeName()).willReturn("publisher.ex1");
    given(MockProducer.publisherWithGivenQueue.getExchangeType()).willReturn(BuiltinExchangeType.FANOUT);
    given(MockProducer.publisherWithGivenQueue.getQueueName()).willReturn("publisher.qu1");
    given(MockProducer.publisherWithGivenQueue.getRoutingKeysBinding())
        .willReturn(List.of("publisher.routing.1", "publisher.routing.2"));

    given(MockProducer.publisherWithoutQueue.getExchangeName()).willReturn("publisher.ex2");
    given(MockProducer.publisherWithoutQueue.getExchangeType()).willReturn(BuiltinExchangeType.DIRECT);
    given(MockProducer.publisherWithoutQueue.getQueueName()).willReturn("");
  }

  @Test
  void whenStartedThenConnectingToBrokerMaySucceedAtFirstTry() throws IOException, TimeoutException {
    given(MockProducer.connectionFactory.newConnection())
        .willReturn(connection);

    cut.contextInitialized(null);

    verify(MockProducer.connectionFactory, timeout(MAXIMUM_TIMEOUT_MILLIS)).newConnection();
  }

  @Test
  void whenStartedThenConnectingToBrokerMaySucceedAfterRetry() throws IOException, TimeoutException {
    given(MockProducer.connectionFactory.newConnection())
        .willThrow(new IOException("Exception from MessagingSetupTest"))
        .willReturn(connection);

    cut.contextInitialized(null);

    verify(MockProducer.connectionFactory, timeout(MAXIMUM_TIMEOUT_MILLIS).atLeast(2)).newConnection();
  }

  @Test
  void whenMessageListenersArePresentThenEachOneIsStartedAsAConsumer() throws IOException {
    Map<String, Object> consumerArgs = Map.of("a", "b");
    given(declareOk.getQueue()).willReturn("givenQueueName");
    given(MockProducer.listenerOfNamedQueue.getConsumerArguments()).willReturn(consumerArgs);
    given(MockProducer.listenerOfUnnamedQueue.getConsumerArguments()).willReturn(consumerArgs);
    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS)).basicQos(1);
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS)).basicQos(2);
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .basicConsume(eq("qu1"), eq(true), eq(consumerArgs),
            refEq(new RabbitConsumer(channel, MockProducer.listenerOfNamedQueue)));
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .basicConsume(eq("givenQueueName"), eq(true), eq(consumerArgs),
            refEq(new RabbitConsumer(channel, MockProducer.listenerOfUnnamedQueue)));
  }

  @Test
  void whenMessageListenerDefinesAQueueNameThenBindsThisQueue() throws IOException {
    given(MockProducer.listenerOfNamedQueue.getQueueNames()).willReturn(List.of("queueName"));

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.listenerOfNamedQueue.getExchangeName(), BuiltinExchangeType.FANOUT, true);
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueDeclare("queueName", true, false, false, Collections.emptyMap());
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueBind("queueName", MockProducer.listenerOfNamedQueue.getExchangeName(), "routing.key.1");
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueBind("queueName", MockProducer.listenerOfNamedQueue.getExchangeName(), "routing.key.2");
  }

  @Test
  void whenMessageListenerDefinesNoQueueNameThenBindsToAnonymousQueue() throws IOException {
    given(declareOk.getQueue()).willReturn("anonymous");

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.listenerOfUnnamedQueue.getExchangeName(), BuiltinExchangeType.TOPIC, true);
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueDeclare();
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueBind("anonymous", MockProducer.listenerOfUnnamedQueue.getExchangeName(), "");
  }

  @Test
  void whenMessageListenerDefinesNoExchangeNameThenDoesNotBind() throws IOException {
    given(MockProducer.listenerOfNamedQueue.getExchangeName()).willReturn("");

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueDeclare("qu1", true, false, false, Collections.emptyMap());
    verify(channel, never()).queueBind(eq("qu1"), any(), any());
  }

  @Test
  void whenBindingOfListenerFailsThenLogsError() throws Exception {
    given(channel.queueBind("qu1",
        MockProducer.listenerOfNamedQueue.getExchangeName(),
        "routing.key.1"))
        .willThrow(IOException.class);

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS)).queueBind("qu1",
        MockProducer.listenerOfNamedQueue.getExchangeName(),
        "routing.key.1");

    // fails only asynchronously
    ExecutionException executionException = assertThrows(ExecutionException.class,
        () -> cut.getConnectionCompletableFuture().get());
    assertThat(executionException.getCause(), instanceOf(UncheckedIOException.class));

    Awaitility.await().atMost(10, TimeUnit.SECONDS)
        .until(() -> logCaptor.getErrorLogs(), hasItem(containsString("Messaging setup not complete")));
  }

  @Test
  void whenMessagePublisherArePresentThenDeclaresEachOnesExchange() throws IOException {

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.publisherWithGivenQueue.getExchangeName(), BuiltinExchangeType.FANOUT, true);
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.publisherWithoutQueue.getExchangeName(), BuiltinExchangeType.DIRECT, true);

  }

  @Test
  void whenMessagePublisherDefinesQueueNameThenBindsItToExchange() throws IOException {

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.publisherWithoutQueue.getExchangeName(), BuiltinExchangeType.DIRECT, true);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueDeclare(MockProducer.publisherWithGivenQueue.getQueueName(), true, false, false, Collections.emptyMap());
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueBind("publisher.qu1", "publisher.ex1", "publisher.routing.1");
    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .queueBind("publisher.qu1", "publisher.ex1", "publisher.routing.2");
  }

  @Test
  void whenMessagePublisherDefinesNoQueueNameThenDoesNotBind() throws IOException {

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS))
        .exchangeDeclare(MockProducer.publisherWithoutQueue.getExchangeName(), BuiltinExchangeType.DIRECT, true);
    verify(channel, never()).queueBind(anyString(), eq("publisher.ex2"), anyString());
  }

  @Test
  void whenBindingOfPublisherFailsThenLogsError() throws IOException {
    given(channel.queueBind(MockProducer.publisherWithGivenQueue.getQueueName(),
        MockProducer.publisherWithGivenQueue.getExchangeName(),
        MockProducer.publisherWithGivenQueue.getRoutingKeysBinding().get(0)))
        .willThrow(IOException.class);

    cut.contextInitialized(null);

    verify(channel, timeout(MAXIMUM_TIMEOUT_MILLIS)).queueBind(MockProducer.publisherWithGivenQueue.getQueueName(),
        MockProducer.publisherWithGivenQueue.getExchangeName(),
        MockProducer.publisherWithGivenQueue.getRoutingKeysBinding().get(0));

    Awaitility.await().atMost(10, TimeUnit.SECONDS)
        .until(() -> logCaptor.getErrorLogs(), hasItem(containsString("Messaging setup not complete")));

    // fails only asynchronously
    ExecutionException executionException = assertThrows(ExecutionException.class,
        () -> cut.getConnectionCompletableFuture().get());
    assertThat(executionException.getCause(), instanceOf(UncheckedIOException.class));
  }

  @Nested
  @ExtendWith({WeldJunit5Extension.class})
  class GivenConnectionIsOpened {
    @WeldSetup
    public WeldInitiator weld = WeldInitiator.from(MessagingSetup.class, MockProducer.class).build();

    @BeforeEach
    void setUp() throws IOException {
      given(connection.isOpen()).willReturn(true);
      cut.contextInitialized(null);
      verify(connection, timeout(MAXIMUM_TIMEOUT_MILLIS).atLeastOnce()).createChannel();
    }

    @Test
    void whenContextIsDestroyedThenClosesConnection() throws IOException {

      cut.contextDestroyed(null);

      verify(connection).close();
    }

    @Test
    void whenConnectionCloseFailsThenThrowsUncheckedException() throws IOException {
      doThrow(IOException.class).when(connection).close();

      assertThrows(UncheckedIOException.class, () -> cut.contextDestroyed(null));

      verify(connection).close();
    }
  }
}
