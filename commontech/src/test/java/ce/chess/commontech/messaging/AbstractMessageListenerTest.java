package ce.chess.commontech.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;

import com.rabbitmq.client.BuiltinExchangeType;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AbstractMessageListenerTest {

  private AbstractMessageListener cut;

  @BeforeEach
  void setup() {
    cut = Mockito.mock(AbstractMessageListener.class, Mockito.CALLS_REAL_METHODS);
  }

  @Test
  void doesReturnEmptyExchangeName() {
    assertThat(cut.getExchangeName(), is(equalTo("")));
  }

  @Test
  void exchangeTypeDefaultsToFanOut() {
    assertThat(cut.getBuiltinExchangeType(), is(equalTo(BuiltinExchangeType.FANOUT)));
  }

  @Test
  void doesReturnEmptyRoutingKeys() {
    assertThat(cut.getRoutingKeysBinding(), contains(List.of("")));
  }

  @Test
  void autoAckDefaultsToFalse() {
    assertThat(cut.isAutoAck(), is(false));
  }

  @Test
  void doesReturnEmptyQueueArguments() {
    assertThat(cut.getQueueArguments(), is(equalTo(Map.of())));
  }

  @Test
  void doesReturnDefaultPrefetchLimit() {
    assertThat(cut.getPrefetchLimit(), is(equalTo(2)));
  }

  @Test
  void doesReturnConsumerBindings() {
    given(cut.getQueueNames()).willReturn(List.of("queue1", "queue2", "queue3"));
    given(cut.getRoutingKeysBinding()).willReturn(List.of(List.of("binding11", "binding12"), List.of("")));

    List<ConsumerBinding> actual = cut.getConsumerBindings();

    List<ConsumerBinding> expected = List.of(
        new ConsumerBinding("queue1", List.of("binding11", "binding12")),
        new ConsumerBinding("queue2", List.of("")),
        new ConsumerBinding("queue3", List.of(""))
    );
    assertThat(actual, contains(expected.toArray()));
  }
}
