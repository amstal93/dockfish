package ce.chess.commontech.messaging;

import com.rabbitmq.client.BuiltinExchangeType;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public abstract class AbstractMessageListener {
  private static final String EMPTY_STRING = "";

  public String getExchangeName() {
    return EMPTY_STRING;
  }

  public BuiltinExchangeType getBuiltinExchangeType() {
    return BuiltinExchangeType.valueOf(getExchangeType());
  }

  public String getExchangeType() {
    return "FANOUT";
  }

  public abstract List<String> getQueueNames();

  public List<List<String>> getRoutingKeysBinding() {
    return List.of(List.of(EMPTY_STRING));
  }

  public boolean isAutoAck() {
    return false;
  }

  public Map<String, Object> getQueueArguments() {
    return Map.of();
  }

  public Map<String, Object> getConsumerArguments() {
    return Map.of();
  }

  public int getPrefetchLimit() {
    return 2;
  }

  public int getMaxRedeliveryAttempts() {
    return 3;
  }

  public abstract void handleMessage(Message message);

  public List<ConsumerBinding> getConsumerBindings() {
    return IntStream
        .range(0, getQueueNames().size())
        .mapToObj(i -> {
          List<String> routingKey;
          if (getRoutingKeysBinding().size() > i) {
            routingKey = getRoutingKeysBinding().get(i);
          } else {
            routingKey = List.of(EMPTY_STRING);
          }
          return new ConsumerBinding(getQueueNames().get(i), routingKey);
        })
        .toList();
  }

}
