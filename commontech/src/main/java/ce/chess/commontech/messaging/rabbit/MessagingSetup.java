package ce.chess.commontech.messaging.rabbit;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.AbstractMessagePublisher;
import ce.chess.commontech.messaging.ConsumerBinding;

import com.google.common.base.Strings;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import com.google.common.util.concurrent.Uninterruptibles;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class MessagingSetup {
  private static final Logger LOGGER = LogManager.getLogger(MessagingSetup.class);

  private static final int CONNECTION_RETRY_INTERVAL_UPPER_BOUND_IN_SECONDS = 30;

  private static final boolean DURABLE_QUEUE = true;
  private static final boolean EXCLUSIVE_QUEUE = false;
  private static final boolean AUTO_DELETE_QUEUE = false;
  private static final boolean DURABLE_EXCHANGE = true;

  @Inject
  SingleConnectionFactory connectionFactory;

  @Inject
  @Any
  Instance<AbstractMessageListener> listeners;

  @Inject
  @Any
  Instance<AbstractMessagePublisher> publishers;

  private Connection connection;

  private CompletableFuture<Connection> connectionCompletableFuture;

  CompletableFuture<Connection> getConnectionCompletableFuture() {
    return connectionCompletableFuture;
  }

  public void contextInitialized(@Observes StartupEvent event) {
    connectionCompletableFuture = CompletableFuture
        .supplyAsync(this::openBrokerConnection)
        .thenApply(this::bindPublishers)
        .thenApply(this::bindListeners);
    connectionCompletableFuture.exceptionally(throwable -> {
      LOGGER.error(() -> "Messaging setup not complete", throwable);
      return null;
    });
  }

  private Connection openBrokerConnection() {
    int retryDelayInSeconds = 1;
    while (null == connection) {
      try {
        connection = connectionFactory.newConnection();
      } catch (TimeoutException | IOException ex) {
        LOGGER.warn("Could not establish connection to {}:{} for {} due to {}",
            connectionFactory.getHost(), connectionFactory.getPort(), connectionFactory.getUsername(), ex.toString());
        Uninterruptibles.sleepUninterruptibly(retryDelayInSeconds, TimeUnit.SECONDS);
        retryDelayInSeconds = Math.min(retryDelayInSeconds, CONNECTION_RETRY_INTERVAL_UPPER_BOUND_IN_SECONDS) * 2;
      }
    }
    LOGGER.info("Connection established to {}:{}", connection.getAddress(), connection.getPort());
    return connection;
  }

  private Connection bindListeners(Connection connection) {
    if (listeners != null) {
      listeners.forEach(this::registerListener);
    }
    return connection;
  }

  private Connection bindPublishers(Connection connection) {
    if (publishers != null) {
      publishers.forEach(this::registerPublisher);
    }
    return connection;
  }

  private void registerListener(AbstractMessageListener listener) {
    listener.getConsumerBindings()
        .forEach(consumerBinding -> connectAndConsume(consumerBinding, listener));
  }

  private void connectAndConsume(ConsumerBinding consumerBinding, AbstractMessageListener listener) {
    LOGGER.info("{}: Binding queue [{}] to exchange [{}] of type {} with routing keys [{}]",
        listener.getClass().getSimpleName(), consumerBinding.getQueueName(), listener.getExchangeName(),
        listener.getBuiltinExchangeType(), consumerBinding.getRoutingKey());
    try {
      Channel channel = createChannel();
      final String queueName = bindConsumerQueueToExchange(channel,
          listener.getExchangeName(), listener.getBuiltinExchangeType(),
          consumerBinding.getQueueName(), consumerBinding.getRoutingKey(),
          listener.getQueueArguments());
      channel.basicQos(listener.getPrefetchLimit());
      RabbitConsumer consumer = new RabbitConsumer(channel, listener);
      channel.basicConsume(queueName, listener.isAutoAck(), listener.getConsumerArguments(), consumer);
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  private void registerPublisher(AbstractMessagePublisher publisher) {
    LOGGER.info("{}: Binding queue [{}] to exchange [{}} of type {} with routing keys [{}]",
        publisher.getClass().getSimpleName(), publisher.getQueueName(), publisher.getExchangeName(),
        publisher.getExchangeType(), publisher.getRoutingKeysBinding());
    bindPublisherExchangeToQueue(publisher.getExchangeName(), publisher.getExchangeType(),
        publisher.getQueueName(), publisher.getRoutingKeysBinding(),
        publisher.getQueueArguments());
  }

  private String bindConsumerQueueToExchange(Channel channel,
                                             String exchangeName, BuiltinExchangeType exchangeType,
                                             String queueName, List<String> routingKeys,
                                             Map<String, Object> queueArguments) throws IOException {
    String declaredQueueName = queueName;
    if (Strings.isNullOrEmpty(queueName)) {
      declaredQueueName = channel.queueDeclare().getQueue();
    } else {
      channel.queueDeclare(queueName, DURABLE_QUEUE, EXCLUSIVE_QUEUE, AUTO_DELETE_QUEUE, queueArguments);
    }
    if (!Strings.isNullOrEmpty(exchangeName)) {
      channel.exchangeDeclare(exchangeName, exchangeType, DURABLE_EXCHANGE);
      for (String routingKey : routingKeys) {
        channel.queueBind(declaredQueueName, exchangeName, routingKey);
      }
    }
    return declaredQueueName;
  }

  private void bindPublisherExchangeToQueue(String exchangeName, BuiltinExchangeType exchangeType,
                                            String queueName, List<String> routingKeys,
                                            Map<String, Object> queueArguments) {
    try (Channel channel = createChannel()) {
      channel.exchangeDeclare(exchangeName, exchangeType, DURABLE_EXCHANGE);
      if (!Strings.isNullOrEmpty(queueName)) {
        channel.queueDeclare(queueName, DURABLE_QUEUE, EXCLUSIVE_QUEUE, AUTO_DELETE_QUEUE, queueArguments);
        for (String routingKey : routingKeys) {
          channel.queueBind(queueName, exchangeName, routingKey);
        }
      }
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    } catch (TimeoutException tex) {
      throw new UncheckedTimeoutException(tex);
    }
  }

  private Channel createChannel() throws IOException {
    return connection.createChannel();
  }

  public void contextDestroyed(@Observes ShutdownEvent event) {
    if (connection != null && connection.isOpen()) {
      try {
        connection.close();
      } catch (IOException ex) {
        throw new UncheckedIOException(ex);
      }
    }
  }
}
