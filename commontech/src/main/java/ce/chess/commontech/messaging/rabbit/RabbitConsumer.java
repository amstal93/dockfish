package ce.chess.commontech.messaging.rabbit;

import ce.chess.commontech.messaging.AbstractMessageListener;
import ce.chess.commontech.messaging.Message;
import ce.chess.commontech.messaging.RequeueException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RabbitConsumer extends DefaultConsumer {
  private static final Logger LOGGER = LogManager.getLogger(RabbitConsumer.class);
  private final AbstractMessageListener listener;

  RabbitConsumer(Channel channel, AbstractMessageListener listener) {
    super(channel);
    this.listener = listener;
  }

  @Override
  public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
      throws IOException {
    LOGGER.info("Consumer {}: Received message {} with Properties {}", consumerTag, envelope, properties);
    Message message = new Message(properties)
        .withExchange(envelope.getExchange())
        .withRoutingKey(envelope.getRoutingKey())
        .withBody(body);

    try {
      listener.handleMessage(message);
    } catch (RequeueException exception) {
      if (!listener.isAutoAck()) {
        LOGGER.warn("Consumer {}: Nacking message {} requeue {}",
            consumerTag, envelope.getDeliveryTag(), !message.isRedelivered(listener.getMaxRedeliveryAttempts()),
            exception);
        getChannel().basicNack(envelope.getDeliveryTag(), false, false);
        redeliver(message);
      }
      return;
    } catch (RuntimeException exception) { // NOPMD
      if (!listener.isAutoAck()) {
        LOGGER.warn("Consumer {}: Nacking message {}", consumerTag, envelope.getDeliveryTag(), exception);
        getChannel().basicNack(envelope.getDeliveryTag(), false, false);
      }
      return;
    }

    if (!listener.isAutoAck()) {
      try {
        getChannel().basicAck(envelope.getDeliveryTag(), false);
      } catch (IOException ex) {
        LOGGER.error("Consumer {}: Message {} was processed but could not be acknowledged",
            consumerTag, envelope.getDeliveryTag(), ex);
        throw ex;
      }
    }
  }

  private void redeliver(Message message) throws IOException {
    if (!message.isRedelivered(listener.getMaxRedeliveryAttempts())) {
      message.setRedelivery();
      getChannel().basicPublish(message.getExchange(), message.getRoutingKey(),
          message.getBasicProperties(), message.getBody());
    }
  }

}
