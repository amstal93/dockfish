package ce.chess.commontech.messaging;

import java.util.List;
import java.util.StringJoiner;

public record ConsumerBinding(String queueName, List<String> routingKey) {

  public String getQueueName() {
    return queueName;
  }

  public List<String> getRoutingKey() {
    return routingKey;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", ConsumerBinding.class.getSimpleName() + "[", "]")
        .add("queueName='" + queueName + "'")
        .add("routingKey=" + routingKey)
        .toString();
  }

}
