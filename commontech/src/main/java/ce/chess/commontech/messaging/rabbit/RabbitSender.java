package ce.chess.commontech.messaging.rabbit;

import ce.chess.commontech.messaging.Message;

import com.google.common.util.concurrent.Uninterruptibles;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ShutdownSignalException;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.microprofile.config.ConfigProvider;

public class RabbitSender implements Closeable {
  private static final Logger LOGGER = LogManager.getLogger(RabbitSender.class);

  private static final int DEFAULT_RETRY_ATTEMPTS = 3;
  private static final int DEFAULT_RETRY_DELAY = 10_000;
  private static final String RETRY_DELAY_PROPERTY = "rabbitmq_sender_retry_delay";

  private final SingleConnectionFactory connectionFactory;
  private Channel channel;

  public RabbitSender(SingleConnectionFactory connectionFactory) {
    this.connectionFactory = connectionFactory;
  }

  public void publish(Message message) throws IOException {
    for (int attempt = 1; attempt <= DEFAULT_RETRY_ATTEMPTS; attempt++) {
      if (attempt > 1) {
        LOGGER.info("Attempt {} to send message", attempt);
      }
      try {
        LOGGER.info("Publishing message to Exchange '{}' with routing key '{}'",
            message.getExchange(), message.getRoutingKey());

        provideChannel()
            .basicPublish(message.getExchange(), message.getRoutingKey(),
                message.getBasicProperties(), message.getBody());
        return;
      } catch (ShutdownSignalException | TimeoutException | IOException ex) {
        handleException(attempt, ex);
      }
    }
  }

  @Override
  public void close() throws IOException {
    if (channel != null && channel.isOpen()) {
      try {
        channel.close();
      } catch (TimeoutException ex) {
        throw new IOException(ex);
      }
    }
  }

  protected void handleException(int attempt, Exception exception) throws IOException {
    try {
      close();
    } catch (IOException ex) {
      LOGGER.warn("Failed to close channel after failed publish", ex);
    }
    channel = null;
    if (attempt == DEFAULT_RETRY_ATTEMPTS) {
      throw new IOException(exception);
    }
    Uninterruptibles.sleepUninterruptibly(getRetryDelay(), TimeUnit.MILLISECONDS);
  }

  @SuppressWarnings("squid:S2095")
  private Channel provideChannel() throws IOException, TimeoutException {
    if (channel == null || !channel.isOpen()) {
      // No problem here with try-with-resouces and open resources:
      // The connection should always remain opened and not be touched by this class.
      // Closing the channel is indeed managed by this class. Sonar does not seem to recognize it.
      channel = connectionFactory.newConnection().createChannel();
    }
    return channel;
  }

  private static int getRetryDelay() {
    return ConfigProvider.getConfig().getOptionalValue(RETRY_DELAY_PROPERTY, Integer.class)
        .orElse(DEFAULT_RETRY_DELAY);
  }

}
