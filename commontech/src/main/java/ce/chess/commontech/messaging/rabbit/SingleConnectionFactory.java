package ce.chess.commontech.messaging.rabbit;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import javax.enterprise.inject.Vetoed;

@Vetoed
public class SingleConnectionFactory extends ConnectionFactory {

  private Connection connection;

  @Override
  public Connection newConnection() throws IOException, TimeoutException {
    if (connection == null || !connection.isOpen()) {
      connection = super.newConnection();
    }
    return connection;
  }
}
