package ce.chess.commontech.resource.jaxrs;

import com.fasterxml.jackson.databind.JsonMappingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class CatchAllExceptionMapper implements ExceptionMapper<Exception> {

  private static final Logger LOG = LogManager.getLogger(CatchAllExceptionMapper.class);

  @Override
  public Response toResponse(Exception exception) {
    if (isIllegalArgument(exception)) {
      LOG.info("Illegal Argument occurred: {}", exception::toString);
      return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    } else if (exception instanceof JsonMappingException) {
      LOG.warn("JsonMappingException occurred: {}", exception::toString);
      return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
    } else if (exception instanceof WebApplicationException) {
      LOG.info("WebApplicationException occurred: {}", exception::toString);
      return ((WebApplicationException) exception).getResponse();
    }
    LOG.error("Exception occurred.", exception);
    return createInternalServerErrorResponse(exception);
  }

  private boolean isIllegalArgument(Throwable exception) {
    return exception instanceof IllegalArgumentException
        || null != exception.getCause() && isIllegalArgument(exception.getCause());
  }

  private static Response createInternalServerErrorResponse(Exception exception) {
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .type(MediaType.APPLICATION_JSON_TYPE)
        .entity(exception.getMessage())
        .build();
  }

}
