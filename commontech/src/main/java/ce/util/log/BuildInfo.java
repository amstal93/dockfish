package ce.util.log;

import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class BuildInfo {
  private static final Logger sLogger = LogManager.getLogger(BuildInfo.class);

  static final String BUILD_INFO_FILE_NAME = "buildinfo.properties";
  private static final String DEFAULT_TEXT = "Revision: <not available>";

  @Inject
  PropertyFileLoader propertyFileLoader;

  private String buildInfoString;

  public String getAsString() {
    if (Strings.isNullOrEmpty(buildInfoString)) {
      Properties properties;
      try {
        properties = propertyFileLoader.loadProperties(BUILD_INFO_FILE_NAME);
        buildInfoString = properties.getProperty("Revision", "Revision?") + ", "
            + properties.getProperty("BuildDate", "BuildDate?") + ", "
            + properties.getProperty("Commit", "Commit?");
      } catch (IllegalArgumentException | IOException ex) {
        sLogger.warn("Error reading build.info {}", ex::toString);
        return DEFAULT_TEXT;
      }
    }
    return buildInfoString;
  }
}
