package ce.util.log;

import io.quarkus.runtime.StartupEvent;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ApplicationScoped
public class StartupVersionLogger {
  private static final Logger sLogger = LogManager.getLogger(StartupVersionLogger.class);

  @Inject
  BuildInfo buildInfo;

  public void contextInitialized(@Observes StartupEvent sce) {
    sLogger.info("Service started: {}", buildInfo::getAsString);
  }

}
