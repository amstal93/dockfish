package ce.util.log;

import com.google.common.io.ByteSource;
import com.google.common.io.Resources;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PropertyFileLoader {

  public Properties loadProperties(String fileName) throws IOException {
    URL url = Resources.getResource(fileName);
    ByteSource byteSource = Resources.asByteSource(url);
    try (InputStream inputStream = byteSource.openBufferedStream()) {
      Properties result = new Properties();
      result.load(inputStream);
      return result;
    }
  }

}
